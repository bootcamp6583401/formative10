package formative.ten;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.mockito.Mock;
import org.junit.Before;

import java.util.Set;
import java.util.HashSet;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Mock
    private Member mockMember1;

    @Mock
    private Member mockMember2;

    @Mock
    private Member mockMember3;

    @Before
    public void setup() {
        mockMember1 = new Member("Kevin Malone");
        mockMember1 = new Member("Michaael Scott");
        mockMember1 = new Member("Jim Halpert");

    }

    @Test
    public void eachMemberWinsOnce() {

        Arisan arisan = new Arisan();
        arisan.addMember(new Member("Jim Halpert"));
        arisan.addMember(new Member("Pam Beesley"));
        arisan.addMember(new Member("Michael Scott"));
        arisan.simulate();

        Set<String> winners = new HashSet<>();
        for (int i = 0; i < arisan.getWinners().size(); i++) {
            winners.add(arisan.getWinners().get(i).getName());
        }

        assertEquals(3, winners.size());
    }

    @Test
    public void eachMemberWinsAmount() {

        Arisan arisan = new Arisan();
        Member member1 = new Member("Jim Halpert");
        Member member2 = new Member("Pam Beesley");
        Member member3 = new Member("Kevin Malone");

        arisan.addMember(member1);
        arisan.addMember(member2);
        arisan.addMember(member3);
        arisan.simulate();

        assertEquals(450000, member1.getTotalWin());
        assertEquals(450000, member2.getTotalWin());
        assertEquals(450000, member3.getTotalWin());

    }

    @Test
    public void noMemberUnpaid() {

        Arisan arisan = new Arisan();
        Member member1 = new Member("Jim Halpert");
        Member member2 = new Member("Pam Beesley");
        Member member3 = new Member("Kevin Malone");

        arisan.addMember(member1);
        arisan.addMember(member2);
        arisan.addMember(member3);
        arisan.simulate();

        assertNotEquals(0, member1.getTotalWin());
        assertNotEquals(0, member2.getTotalWin());
        assertNotEquals(0, member3.getTotalWin());

    }
}
