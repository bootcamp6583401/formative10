package formative.ten;

public class Member {
    private String name;
    private boolean hasReceivedCode = false;
    private String receivedCode;
    private int winTotal = 0;

    public Member(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasReceivedCode() {
        return hasReceivedCode;
    }

    public void setHasReceivedCode(boolean hasReceivedCode) {
        this.hasReceivedCode = hasReceivedCode;
    }

    public String getReceivedCode() {
        return receivedCode;
    }

    public void setReceivedCode(String receivedCode) {
        this.receivedCode = receivedCode;
    }

    public void win(int total) {
        this.winTotal += total;
    }

    public int getTotalWin() {
        return winTotal;
    }
}
