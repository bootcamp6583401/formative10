package formative.ten;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Arisan arisan = new Arisan();
        arisan.addMember(new Member("Robert California"));
        arisan.addMember(new Member("Kevin Malone"));
        arisan.addMember(new Member("Jim Halpert"));
        arisan.addMember(new Member("Pam Beesly"));
        arisan.addMember(new Member("Dwight Schrute"));
        arisan.addMember(new Member("Michael Scott"));
        arisan.addMember(new Member("Stanley Hudson"));
        arisan.addMember(new Member("Angela Martin"));
        arisan.addMember(new Member("Oscar Martinez"));
        arisan.addMember(new Member("Phyllis Vance"));
        arisan.addMember(new Member("Creed Bratton"));
        arisan.addMember(new Member("Kelly Kapoor"));
        arisan.addMember(new Member("Ryan Howard"));
        arisan.addMember(new Member("Meredith Palmer"));
        arisan.addMember(new Member("Toby Flenderson"));

        arisan.simulate();

    }
}
