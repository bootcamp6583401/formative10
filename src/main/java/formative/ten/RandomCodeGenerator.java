package formative.ten;

import java.util.ArrayList;
import java.util.Random;

public class RandomCodeGenerator {
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final Random RANDOM = new Random();

    private static ArrayList<String> usedCodes = new ArrayList<>();

    public static String generateRandomCode() {
        boolean uniqueCodeCreated = false;
        StringBuilder code = new StringBuilder();

        while (!uniqueCodeCreated) {
            code.delete(0, code.length());
            for (int i = 0; i < 5; i++) {

                int randomIndex = RANDOM.nextInt(CHARACTERS.length());
                code.append(CHARACTERS.charAt(randomIndex));
            }

            if (!usedCodes.contains(code.toString())) {
                uniqueCodeCreated = true;
                usedCodes.add(code.toString());
            }
        }

        return code.toString();
    }

}
