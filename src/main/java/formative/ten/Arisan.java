package formative.ten;

import java.util.ArrayList;
import java.util.Random;

public class Arisan {
    private ArrayList<Member> members = new ArrayList<>();
    private static final Random RANDOM = new Random();

    public static final int DEFAULT_CONTRIBUTION = 150000;

    private ArrayList<String> currentCodeList = new ArrayList<>();

    private int currentPool = 0;

    private ArrayList<Member> winners = new ArrayList<>();

    public Arisan() {

    }

    public void simulate() {
        int originalSize = members.size();
        // 1 iter = 1 meet
        for (int i = 0; i < originalSize; i++) {
            System.out.println("MEETING " + (i + 1));
            for (int j = 0; j < originalSize; j++) {
                currentPool += DEFAULT_CONTRIBUTION;
            }
            shareCode();
            String winnerCode = pickCode();
            System.out.println("Winner Code picked! \"" + winnerCode + "\"");
            System.out.println("Winner of meeting " + (i + 1));
            System.out.println("Received Rp." + currentPool);
            Member winner = pickWinner(winnerCode);
            winners.add(winner);
            winner.win(currentPool);
            members.remove(winner);
            System.out.println("\n");
            reset();
        }
    }

    public void shareCode() {
        for (int i = 0; i < members.size(); i++) {
            String generatedCode = RandomCodeGenerator.generateRandomCode();
            currentCodeList.add(generatedCode);
            members.get(i).setReceivedCode(generatedCode);
            members.get(i).setHasReceivedCode(true);
        }
        System.out.println("Finished handing out codes!");
    }

    public String pickCode() {
        System.out.println("Picking winner code... Total codes: " + currentCodeList.size());
        int randomIndex = RANDOM.nextInt(currentCodeList.size());
        return currentCodeList.get(randomIndex);
    }

    public Member pickWinner(String winnerCode) {
        Member winner = null;
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getReceivedCode().equals(winnerCode)) {
                winner = members.get(i);
            }
        }

        System.out.println("Winner is " + winner.getName() + "!");
        return winner;
    }

    public void reset() {
        for (int i = 0; i < members.size(); i++) {
            members.get(i).setHasReceivedCode(false);
            members.get(i).setReceivedCode(null);
        }
        currentCodeList.clear();
        currentPool = 0;
    }

    public void addMember(Member member) {
        members.add(member);
    }

    public ArrayList<Member> getWinners() {
        return winners;
    }
}
